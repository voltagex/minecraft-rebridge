package org.voltagex.exampleextendedmod;

import org.voltagex.rebridge.api.annotations.Controller;

@Controller
public class AnotherTestController
{
    public AnotherTestController()
    {

    }

    public String getValue()
    {
        return "A value";
    }
}
